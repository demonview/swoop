\ gforth harness for SWOOP
\ derived from VFXHarness

: create-xt ( "name" -- xt )
  create lastxt ;

0 constant location

: -ORDER ( wid -- )
\ *G *\fo{-ORDER} removes all references to WID from the context
\ ** list.
   >R  GET-ORDER R> ( wid wid ... wid n wid)
   0 >R BEGIN
      SWAP ?DUP WHILE 1- SWAP ( wid...wid n wid)
      ROT 2DUP = IF DROP ELSE
         R> SWAP >R 1+ >R
      THEN
   REPEAT DROP
   R> DUP BEGIN ( n n)
      ?DUP WHILE 1-
      R> -ROT
   REPEAT SET-ORDER ;

: +order ( wid -- )
  dup -order >order ;

: package	\ "name" -- parent package
\ *G Creates a new package if it does not exist, or opens an
\ ** existing one.
  get-current  >in @  bl word find if	\ -- parent >in xt ; package already exists
    nip  execute			\ -- parent package
  else					\ -- parent >in text ; package does not exist
    drop >in !  wordlist dup constant	\ -- parent package
  then
  dup set-current  dup +order
;

: end-package	\ parent package --
\ *G Close the current package
  -order set-current  ;

: Public	\ parent package -- parent package
\ *G Words defined after *\fo{Public} will be in the parent of
\ ** the package.
  over set-current  ;

: Private	\ --
\ *G Words defined after *\fo{Private} are added to the package
\ ** wordlist, which is normally unavailable.
  dup set-current  ;

: cell- ( x -- x' )  cell - ;

: @+		\ addr -- addr+cell x
  dup cell +  swap @  ;

: !+		\ addr x -- addr+cell
  over !  cell +  ;

: /ALLOT	\ n --
  HERE SWAP DUP ALLOT ERASE ;

: LINKS ( a -- a' )
   BEGIN  DUP @ ?DUP WHILE  NIP  REPEAT ;

: >LINK ( a -- )
   here  OVER @ ,  SWAP ! ;

: <LINK ( a -- )   LINKS  >LINK ;

user 'This	\ -- addr
\ *G Holds the handle of the current class.
user 'Self	\ -- addr
\ *G Holds the handle of the current object.

s" Not an object" exception constant IOR_OOP_NOTOBJ
s" No member (resolve)" exception constant IOR_OOP_NORESOLVE
s" No member (sendmsg)" exception constant IOR_OOP_NOSENDMSG
s" No member (calling)" exception constant IOR_OOP_NOCALLING
s" Not a member" exception constant IOR_OOP_NOTMEMBER

: ?throw ( -1 ior -- ??? ) ( 0 ior -- )
  swap if throw else drop then ;
