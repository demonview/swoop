\ SFharness.fth - SwiftForth harness for SWOOP

THROW#
   S" Not an object"            >THROW ENUM IOR_OOP_NOTOBJ
   S" No member (resolve)"      >THROW ENUM IOR_OOP_NORESOLVE
   S" No member (sendmsg)"      >THROW ENUM IOR_OOP_NOSENDMSG
   S" No member (calling)"      >THROW ENUM IOR_OOP_NOCALLING
   S" Not a member"             >THROW ENUM IOR_OOP_NOTMEMBER
TO THROW#

: ProcessWord ( $ -- )
   COUNT  STATE @ IF WORD-COMPILER ELSE WORD-INTERPRETER THEN  ;

: ms@  ( -- ms )  counter  ;
