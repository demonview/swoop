\ VFX Forth harness for SWOOP

((
Copyright (c) 2009
MicroProcessor Engineering
133 Hill Lane
Southampton SO15 5AF
England

tel:   +44 (0)23 8063 1441
fax:   +44 (0)23 8033 9691
email: mpe@mpeforth.com
       tech-support@mpeforth.com
web:   http://www.mpeforth.com
Skype: mpe_sfp

From North America, our telephone and fax numbers are:
       011 44 23 8063 1441
       011 44 23 8033 9691
       901 313 4312 (North American access number to UK office)


To do
=====

Change history
==============
))

only forth definitions
decimal

\ =============
\ *! vfxharness
\ *T SWOOP harness for VFX Forth
\ =============

\ ***************
\ *S Control flow
\ ***************

: ?exit		\ flag --
\ *G Perform *\fo{EXIT} if *\i{flag} is non-zero.
  ?comp  postpone if  postpone exit  postpone then  ; immediate


\ **********************
\ *S Forth word handlers
\ **********************

: CREATE-XT ( "name" -- xt )
\ *G ANS provides no way for a created word to know its own xt -- which is
\ ** needed for portability in our object package. CREATE-XT provides a
\ ** means to CREATE an entity in the dictionary that knows its own xt.
  create here body>  ;

(( \ ANS portable version
: CREATE-XT ( -- xt )
   >IN @  CREATE  >IN !
   BL WORD COUNT GET-CURRENT SEARCH-WORDLIST 0= THROW ;
))

: location	\ -- x
\ *G Return source file location information, or zero if your
\ ** system does not support *\fo{LOCATE}.
  line# @ $FFFF min  Optimising @
  if  $0001:0000 or then
;

Synonym string, $,	\ caddr len --
\ *G Lay a string into the dictionary as a counted string.

: .id		\ nfa --
\ *G Display the name of a word whose NFA is given.
  .name  ;


\ ***************
\ *S Search order
\ ***************
\ *P The SwiftForth *\fo{PACKAGE} mechanism is more elaborate than
\ ** this one. A *\fo{PACKAGE} is a named wordlist.

: -ORDER ( wid -- )
\ *G *\fo{-ORDER} removes all references to WID from the context
\ ** list.
   >R  GET-ORDER R> ( wid wid ... wid n wid)
   0 >R BEGIN
      SWAP ?DUP WHILE 1- SWAP ( wid...wid n wid)
      ROT 2DUP = IF DROP ELSE
         R> SWAP >R 1+ >R
      THEN
   REPEAT DROP
   R> DUP BEGIN ( n n)
      ?DUP WHILE 1-
      R> -ROT
   REPEAT SET-ORDER ;

: +ORDER ( wid -- )
\ *G *\fo{+ORDER} guarantees that the WID is in the context list
\ ** only once and first in the search order.
   DUP -ORDER  >R GET-ORDER R> SWAP 1+ SET-ORDER ;

: package	\ "name" -- parent package
\ *G Creates a new package if it does not exist, or opens an
\ ** existing one.
  get-current  >in @  bl word find if	\ -- parent >in xt ; package already exists
    nip  execute			\ -- parent package
  else					\ -- parent >in text ; package does not exist
    drop >in !  wordlist dup constant	\ -- parent package
  then
  dup set-current  dup +order
;

: end-package	\ parent package --
\ *G Close the current package
  -order set-current  ;

: Public	\ parent package -- parent package
\ *G Words defined after *\fo{Public} will be in the parent of
\ ** the package.
  over set-current  ;

: Private	\ --
\ *G Words defined after *\fo{Private} are added to the package
\ ** wordlist, which is normally unavailable.
  dup set-current  ;


\ ********************
\ *S Memory operations
\ ********************
\ *P The original host Forth (SwiftForth) uses a form of relative
\ ** addressing for linked lists. If you do not need to impose
\ ** relative address the words ending in *\fo{REL} can be
\ ** replaced by their absolute equivalents.

: @+		\ addr -- addr+cell x
  dup cell +  swap @  ;

: !+		\ addr x -- addr+cell
  over !  cell +  ;

: /ALLOT	\ n --
  HERE SWAP DUP ALLOT ERASE ;

: LINKS ( a -- a' )
   BEGIN  DUP @ ?DUP WHILE  NIP  REPEAT ;

: >LINK ( a -- )
   here  OVER @ ,  SWAP ! ;

: <LINK ( a -- )   LINKS  >LINK ;

cell +user 'This	\ -- addr
\ *G Holds the handle of the current class.
cell +user 'Self	\ -- addr
\ *G Holds the handle of the current object.


\ **************
\ *S Error codes
\ **************

ErrDef IOR_OOP_NOTOBJ		"Not an object"
ErrDef IOR_OOP_NORESOLVE	"No member (resolve)"
ErrDef IOR_OOP_NOSENDMSG  	"No member (sendmsg)"
ErrDef IOR_OOP_NOCALLING	"No member (calling)"
ErrDef IOR_OOP_NOTMEMBER	"Not a member"


\ ****************
\ *S Miscellaneous
\ ****************

[defined] Target_386_Windows [if]
: ms@		\ -- ms
\ *G Return the contents of free-running millisecond counter.
  GetTickCount  ;
[then]


\ ======
\ *> ###
\ ======

decimal
